package main

import (
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/joho/godotenv"
	"github.com/scaleway/scaleway-sdk-go/api/instance/v1"
	"github.com/scaleway/scaleway-sdk-go/api/vpc/v1"
	"github.com/scaleway/scaleway-sdk-go/logger"
	"github.com/scaleway/scaleway-sdk-go/scw"
)

const (
	prefixName     = "test"
	commercialType = "DEV1-L"
	image          = "debian_bullseye"
	quantity       = 6
)

var (
	tags = []string{"test"}
)

func init() {
	logger.EnableDebugMode()

	err := godotenv.Load()
	if err != nil {
		logger.Errorf("Error loading .env file")
	}
}

func main() {
	// Initialize Scaleway client
	client, err := scw.NewClient(scw.WithEnv())
	if err != nil {
		logger.Errorf("%v", err)
		os.Exit(1)
	}

	// Scaleway VPC API
	vpcAPI := vpc.NewAPI(client)

	// Create private network
	createPrivateNetwork, err := vpcAPI.CreatePrivateNetwork(&vpc.CreatePrivateNetworkRequest{
		Name: prefixName,
		Tags: tags,
	})
	if err != nil {
		logger.Errorf("%v", err)
		os.Exit(1)
	}

	// Create all instance with GoRoutine
	var wg sync.WaitGroup
	for i := 0; i < quantity; i++ {
		wg.Add(1)
		go func(wg *sync.WaitGroup, id int, vpcID string) {
			defer wg.Done()
			if err := createInstance(client, id+1, vpcID); err != nil {
				logger.Errorf("%v", err)
			}
		}(&wg, i, createPrivateNetwork.ID)
	}
	wg.Wait()
}

func createInstance(client *scw.Client, id int, vpcID string) error {
	// Scaleway Instance API
	instanceAPI := instance.NewAPI(client)

	// Create instance
	createInstance, err := instanceAPI.CreateServer(&instance.CreateServerRequest{
		Name:              fmt.Sprintf("%s-%d", prefixName, id),
		DynamicIPRequired: scw.BoolPtr(false),
		CommercialType:    commercialType,
		Image:             image,
		Tags:              tags,
	})
	if err != nil {
		return err
	}

	// Create private NIC
	if _, err := instanceAPI.CreatePrivateNIC(&instance.CreatePrivateNICRequest{
		ServerID:         createInstance.Server.ID,
		PrivateNetworkID: vpcID,
	}); err != nil {
		return err
	}

	// Start instance
	timeout := time.Minute * 5
	return instanceAPI.ServerActionAndWait(&instance.ServerActionAndWaitRequest{
		ServerID: createInstance.Server.ID,
		Action:   instance.ServerActionPoweron,
		Timeout:  &timeout,
	})
}
