module gitlab.com/tfournier/debug-scaleway-api

go 1.16

require (
	github.com/joho/godotenv v1.4.0
	github.com/scaleway/scaleway-sdk-go v1.0.0-beta.7
)
